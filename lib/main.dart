import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget ProfileSection = Container(
    padding: EdgeInsets.all(32),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
            child: Column(
          children: [
            Image.asset(
              'images/getstudentimage.jpg',
              scale: 0.9,
            ),
          ],
        )),
        Expanded(
            child: Column(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Name : Mr. Porramet Auttalotai",
                  style: TextStyle(fontSize: 25),
                ),
                Text("Age : 21 years", style: TextStyle(fontSize: 25)),
                Text("Gender : male", style: TextStyle(fontSize: 25))
              ],
            )
          ],
        ))
      ],
    ),
  );

  Widget ImageSection = Container(
    padding: EdgeInsets.all(32),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Image.network(
          'https://image.flaticon.com/icons/png/512/919/919826.png',
          width: 100,
          height: 100,
        ),
        Image.network(
          'https://image.flaticon.com/icons/png/512/888/888859.png',
          width: 100,
          height: 100,
        ),
        Image.network(
          'https://image.flaticon.com/icons/png/512/541/541509.png',
          width: 100,
          height: 100,
        ),
        Image.network(
          'https://image.flaticon.com/icons/png/512/226/226777.png',
          width: 100,
          height: 100,
        ),
        Image.network(
          'https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Vue.js_Logo_2.svg/1200px-Vue.js_Logo_2.svg.png',
          width: 100,
          height: 100,
        ),
      ],
    ),
  );

  Widget HistorySection = Container(
    padding: EdgeInsets.all(32),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
            child: Column(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Birthday : 2000 february 23",
                    style: TextStyle(fontSize: 25)),
                Text("Address : 487/33 Praksa Street,Samutprakarn 10280",
                    style: TextStyle(fontSize: 25)),
                Text("Tel : 0804479242", style: TextStyle(fontSize: 25)),
                Text("My hobby : read light novel,listen to music",
                    style: TextStyle(fontSize: 25))
              ],
            ),
          ],
        ))
      ],
    ),
  );

  Widget EducationSection = Container(
    padding: EdgeInsets.all(32),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          children: [
            Image.network(
              'https://img-premium.flaticon.com/png/512/3074/premium/3074058.png?token=exp=1627198619~hmac=5651d9044a057593f43ac6c5846f171b',
              width: 100,
              height: 100,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("2018 - Present", style: TextStyle(fontSize: 25)),
                Text("  Burapha University", style: TextStyle(fontSize: 25)),
                Text("    - Computer Science", style: TextStyle(fontSize: 25)),
                Text("2012 - 2018", style: TextStyle(fontSize: 25)),
                Text("  Debsirin Samutprakan", style: TextStyle(fontSize: 25)),
              ],
            )
          ],
        )
      ],
    ),
  );

  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Resume',
        home: Scaffold(
          body: ListView(
            children: [
              ProfileSection,
              ImageSection,
              HistorySection,
              EducationSection
            ],
          ),
        ));
  }
}
